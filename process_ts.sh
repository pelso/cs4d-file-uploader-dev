#!/usr/bin/env bash

docker run -it --rm \
  -u "$(id -u):$(id -g)" \
  -v /home/kornel/encrypted/projects/cs4d-file-uploader:/workspace \
  harmish/typescript tsc -p /workspace
<?php

$output = json_encode( [
	'processed-at' => (new \Datetime())->format('c'),
	'files' => $_FILES,
	'post'  => $_POST
] );
header("Content-Length: " . strlen($output));
echo $output;
